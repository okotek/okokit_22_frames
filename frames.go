package frames

import (
	"image"
	"time"

	"gocv.io/x/gocv"
)

//Frame holds the elements of a deconstructed gocv mat and related metadata
type Frame struct {
	ImDat           []byte // <- Byte data from gocv mat
	Width           int    // <- Yeah...
	Height          int
	Typ             gocv.MatType
	Channels        int
	MotionLocations map[image.Rectangle]float64 // <- Rectangles around motion and how much that rectangle changed since last frame
	ClassifierLocs  map[image.Rectangle]string  // <- Locations and labels of classifier detections
	ImTime          uint64                      // <- Closest reference in unix nano timestamp that the image was taken
	Camid           string                      // <- ID of the camera
	Unitid          string
	Uname           string
	Upass           []byte // <- Bcrypt output of password hash
	Imgname         []byte // <- SHA256 of the image's data to act as an identifier
}

//Generate a blank frame. Accepts byte slices that represent username and the password hash.
func GenFrame(inp ...[]byte) Frame {

	var uname string
	var upass []byte
	var camid string
	var unitd string
	var shash []byte

	mlocs := make(map[image.Rectangle]float64)
	clocs := make(map[image.Rectangle]string)

	if len(inp) > 0 {
		uname = string(inp[0])
	}

	if len(inp) > 1 {
		upass = inp[1]
	}

	if len(inp) > 2 {
		camid = string(inp[2])
	}

	if len(inp) > 3 {
		unitd = string(inp[3])
	}

	retFrame := Frame{
		[]byte{},
		0,
		0,
		0,
		0,
		mlocs,
		clocs,
		uint64(time.Now().UnixNano()),
		camid,
		unitd,
		uname,
		upass,
		shash,
	}

	return retFrame
}

/*
	ConsumeMat allows a frame to acept a gocv.Mat as an input
	and breaks it down into elements of that frame. It also resets
	the time element to the time that the mat is consumed.

	NOTE: Implement error checking.
*/
func (f *Frame) ConsumeMat(m gocv.Mat, inp ...[]byte) error {

	mlocs := make(map[image.Rectangle]float64)
	clocs := make(map[image.Rectangle]string)

	if len(inp) > 0 {
		f.Uname = string(inp[0])
	}

	if len(inp) > 1 {
		f.Upass = inp[1]
	}

	if len(inp) > 2 {
		f.Camid = string(inp[2])
	}

	if len(inp) > 3 {
		f.Unitid = string(inp[3])
	}

	f.ImDat = m.ToBytes()
	f.Width = m.Cols()
	f.Height = m.Rows()
	f.Typ = m.Type()
	f.Channels = m.Channels()
	f.MotionLocations = mlocs
	f.ClassifierLocs = clocs

	f.ImTime = uint64(time.Now().UnixNano())

	return nil
}

//Deconstruct a frame to a mat and its constituent componets. NOTE: Return more parts later.
func (f *Frame) Deconstruct() (gocv.Mat, string, []byte, error) {
	outMat, err := gocv.NewMatFromBytes(f.Height, f.Width, f.Typ, f.ImDat)
	if err != nil {
		return outMat, "", []byte(""), err
	}

	return outMat, f.Uname, f.Upass, err
}

/*
	DrawSquares uses the motion detection and object classification
	maps and draws squares ove those elements in the image.
*/
func (f *File) DrawSquares() error {
	tmpMat, _, _, _ := f.Deconstruct()

}
